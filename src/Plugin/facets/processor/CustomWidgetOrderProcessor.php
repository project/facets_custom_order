<?php

namespace Drupal\facets_custom_order\Plugin\facets\processor;

use Drupal\Core\Cache\UnchangingCacheableDependencyTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\Result\Result;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\SortProcessorInterface;
use Drupal\facets\Processor\SortProcessorPluginBase;

/**
 * A processor that sorts the facet values in a custom order.
 *
 * @FacetsProcessor(
 *   id = "custom_widget_order",
 *   label = @Translation("Sort by custom order"),
 *   description = @Translation("Sorts the facet in a predifined custom order."),
 *   stages = {
 *     "sort" = 40
 *   }
 * )
 */
class CustomWidgetOrderProcessor extends SortProcessorPluginBase implements SortProcessorInterface {

  use UnchangingCacheableDependencyTrait;

  /**
   * The custom order as flipped array.
   *
   * @var array
   */
  protected $order = [];

  /**
   * The amount of custom order values.
   *
   * @var int
   */
  protected $count;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {

    $processors = $facet->getProcessors();
    $config = isset($processors[$this->getPluginId()]) ? $processors[$this->getPluginId()] : NULL;

    $build['custom_order'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom order'),
      '#description' => $this->t('Enter the order for the facet, one item per line. All items not listed here get added at the end in random order.'),
      '#default_value' => !is_null($config) ? $config->getConfiguration()['custom_order'] : '',
    ];

    $build['display_values'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use display values'),
      '#description' => $this->t('Use the display values to define the order else raw values will be used.'),
      '#default_value' => !is_null($config) ? $config->getConfiguration()['display_values'] : NULL,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function sortResults(Result $a, Result $b) {

    $config = $this->getConfiguration();
    $order = $this->getOrder();
    $count = $this->getCount();

    $a_value = $config['display_values'] ? $a->getDisplayValue() : $a->getRawValue();
    $b_value = $config['display_values'] ? $b->getDisplayValue() : $b->getRawValue();

    return ($order[$a_value] ?? $count) <=> ($order[$b_value] ?? $count);
  }

  /**
   * Gets the amount of values in custom order.
   */
  protected function getCount() {
    if (empty($this->order)) {
      $this->getOrder();
    }

    if (!is_null($this->count)) {
      return $this->count;
    }

    $this->count = count($this->order);

    return $this->count;
  }

  /**
   * Gets the custom order from config and flips it.
   */
  protected function getOrder() {
    if (!empty($this->order)) {
      return $this->order;
    }

    $config = $this->getConfiguration();

    if (isset($config['custom_order'])) {
      $order = array_map('trim', explode(PHP_EOL, $config['custom_order']));
      $order = array_flip($order);
    }

    $this->order = $order;

    return $this->order;
  }

}
