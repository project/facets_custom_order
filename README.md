INTRODUCTION
------------

This module makes it possible to define a custom order for a facet.

This is especially useful, when the facet items have no wight or other value by wich they could get sorted or if the facet items have no single source, like in aggregated index fields.
REQUIREMENTS

This module requires the following modules:
* Facets (https://www.drupal.org/project/facets)

INSTALLATION
------------

* Install as you would normally install a Drupal module contributed.

CONFIGURATION
-------------

* Select "Sort by custom order" in "Facet sorting" configuration in the facets config filter.
